<?php


namespace Drupal\ivorycoast_address\EventSubscriber;

use Drupal\address\Event\AddressEvents;
use Drupal\address\Event\AddressFormatEvent;
use Drupal\address\Event\SubdivisionsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class IvoryCoastEventSubscriber implements EventSubscriberInterface{

  public static function getSubscribedEvents() {
    $events[AddressEvents::ADDRESS_FORMAT][] = ['onAddressFormat'];
    $events[AddressEvents::SUBDIVISIONS][] = ['onSubdivisions'];
    return $events;
  }

  public function onAddressFormat(AddressFormatEvent $event) {
    $definition = $event->getDefinition();
    if ($definition['country_code'] == 'CI') {
      $definition['format'] =  "\n%administrativeArea\n%locality\n%dependentLocality\n%addressline1";
      $definition['required_fields'][] = "locality";
      $definition['required_fields'][] = "dependentLocality";
      $definition['subdivision_depth'] = 3;
      $event->setDefinition($definition);
    }
  }

  public function onSubdivisions(SubdivisionsEvent $event) {
    $parents = $event->getParents();

    if ($parents == ['CI']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'CI-D-ABJ' => ['name' => 'District Autonome d’Abidjan', 'has_children' => TRUE],
          'CI-D-YAM' => ['name' => 'District Autonome de Yamoussoukro', 'has_children' => TRUE],
          'CI-D-BAS' => ['name' => 'District du Bas-Sassandra', 'has_children' => TRUE],
          'CI-D-COE' => ['name' => 'District de la Comoé', 'has_children' => TRUE],
          'CI-D-DEN' => ['name' => 'District du Denguélé', 'has_children' => TRUE],
          'CI-D-GOH' => ['name' => 'District du Gôh-Djiboua', 'has_children' => TRUE],
          'CI-D-LAC' => ['name' => 'District des Lacs', 'has_children' => TRUE],
          'CI-D-LAG' => ['name' => 'District des Lagunes', 'has_children' => TRUE],
          'CI-D-MON' => ['name' => 'District des Montagnes', 'has_children' => TRUE],
          'CI-D-SAM' => ['name' => 'District de Sassandra-Marahoué', 'has_children' => TRUE],
          'CI-D-SAV' => ['name' => 'District des Savanes', 'has_children' => TRUE],
          'CI-D-BAN' => ['name' => 'District de la Vallée du Bandama', 'has_children' => TRUE],
          'CI-D-WOR' => ['name' => 'District du Woroba', 'has_children' => TRUE],
          'CI-D-ZAN' => ['name' => 'District du Zanzan', 'has_children' => TRUE],
        ]
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['CI', 'CI-D-ABJ']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'CI-ABJ-ABO' => ['name'=> 'Abobo', 'has_children' => TRUE],
          'CI-ABJ-ADJ' => ['name'=> 'Adjamé', 'has_children' => TRUE],
          'CI-ABJ-ANY' => ['name'=> 'Anyama', 'has_children' => TRUE],
          'CI-ABJ-ATT' => ['name'=> 'Attécoubé', 'has_children' => TRUE],
          'CI-ABJ-BIN' => ['name'=> 'Bingerville', 'has_children' => TRUE],
          'CI-ABJ-COC' => ['name'=> 'Cocody', 'has_children' => TRUE],
          'CI-ABJ-KOU' => ['name'=> 'Koumassi', 'has_children' => TRUE],
          'CI-ABJ-MAR' => ['name'=> 'Marcory', 'has_children' => TRUE],
          'CI-ABJ-PLA' => ['name'=> 'Plateau', 'has_children' => TRUE],
          'CI-ABJ-POR' => ['name'=> 'Port-Bouet', 'has_children' => TRUE],
          'CI-ABJ-SON' => ['name'=> 'Songon', 'has_children' => TRUE],
          'CI-ABJ-TRE' => ['name'=> 'Treichville', 'has_children' => TRUE],
          'CI-ABJ-YOP' => ['name'=> 'Yopougon', 'has_children' => TRUE],
        ]
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['CI', 'CI-INT']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'CI-INT-ABO' => ['name'=> 'Aboisso', 'has_children' => TRUE],
          'CI-INT-BOU' => ['name'=> 'Bouaké', 'has_children' => TRUE],
        ]
      ];
      $event->setDefinitions($definitions);
    }

//    if ($parents == ['CI', 'CI-ABJ', 'Badung']) {}
//    if ($parents == ['CI', 'CI-ABJ', 'Badung']) {}

  }
}
